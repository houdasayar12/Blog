
rm -rf vendor
composer install
composer update
chmod -R 777 ./
rm -f ./migrations/Version*
php bin/console doctrine:database:create --if-not-exists
php bin/console make:migration
php bin/console doctrine:migrations:migrate --no-interaction
php bin/console doctrine:schema:validate
composer require annotations
#php /bin/console doctrine:fixtures:load -n;
#php /bin/phpunit;
#composer fund
exec apache2-foreground
